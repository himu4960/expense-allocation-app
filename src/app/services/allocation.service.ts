import { Injectable } from '@angular/core';
import { Employee } from '../models/employee.model';

@Injectable({
  providedIn: 'root',
})
export class AllocationService {
  rootEmployee: Employee;

  constructor() {
    this.rootEmployee = new Employee('PM A', 'projectManager', 300);
  }

  addDeveloper(manager: Employee) {
    manager.children.push(new Employee('Developer', 'developer', 1000));
  }

  addQATester(manager: Employee) {
    manager.children.push(new Employee('QA Tester', 'qaTester', 500));
  }

  addProjectManager(manager: Employee) {
    manager.children.push(new Employee('PM', 'projectManager', 300));
  }

  calculateTotalCost(employee: Employee): number {
    let totalCost = employee.cost;

    for (const child of employee.children) {
      totalCost += this.calculateTotalCost(child);
    }

    return totalCost;
  }
}
