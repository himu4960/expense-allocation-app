import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ExpenseAllocationComponent } from './expense-allocation.component';

describe('ExpenseAllocationComponent', () => {
  let component: ExpenseAllocationComponent;
  let fixture: ComponentFixture<ExpenseAllocationComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ExpenseAllocationComponent]
    });
    fixture = TestBed.createComponent(ExpenseAllocationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
