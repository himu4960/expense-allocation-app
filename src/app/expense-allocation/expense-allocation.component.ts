import { Component } from '@angular/core';
import { Employee } from '../models/employee.model';
import { AllocationService } from '../services/allocation.service';

@Component({
  selector: 'app-expense-allocation',
  templateUrl: './expense-allocation.component.html',
  styleUrls: ['./expense-allocation.component.css']
})
export class ExpenseAllocationComponent {
  rootEmployee: Employee;
  totalCost: number;

  constructor(private allocationService: AllocationService) {
    this.rootEmployee = allocationService.rootEmployee;
    this.totalCost = allocationService.calculateTotalCost(this.rootEmployee);
  }

  addDeveloper() {
    this.allocationService.addDeveloper(this.rootEmployee);
    this.totalCost = this.allocationService.calculateTotalCost(this.rootEmployee);
  }

  addQATester() {
    this.allocationService.addQATester(this.rootEmployee);
    this.totalCost = this.allocationService.calculateTotalCost(this.rootEmployee);
  }

  addProjectManager() {
    this.allocationService.addProjectManager(this.rootEmployee);
    this.totalCost = this.allocationService.calculateTotalCost(this.rootEmployee);
  }
}
