export class Employee {
  name: string;
  type: string;
  cost: number;
  children: Employee[];

  constructor(name: string, type: string, cost: number) {
    this.name = name;
    this.type = type;
    this.cost = cost;
    this.children = [];
  }
}
  

